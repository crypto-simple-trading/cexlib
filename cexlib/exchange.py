from abc import ABC, abstractmethod
from typing import Dict

class Exchange(ABC):
    @abstractmethod
    def getAllPrices(self) -> Dict[str, float]:
        pass

    @abstractmethod
    def getAccountBalance(self) -> Dict[str, float]:
        pass

    @abstractmethod
    def swap(self, token1:str, token2:str, value1:float):
        pass

    def getPrice(self, token1:str, token2:str) -> float:
        prices = self.getAllPrices()
        try:
            price = prices[f"{token1}{token2}"]
        except:
            price = 1 / prices[f"{token2}{token1}"]
        return price
