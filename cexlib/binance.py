from typing import Dict
from overrides import overrides
from pathlib import Path
from binance_requests import BinanceClient
from .exchange import Exchange

class Binance(Exchange):
    def __init__(self, secretFilePath:Path):
        self.client = BinanceClient(secretFilePath=secretFilePath)
    
    @overrides
    def getAllPrices(self) -> Dict[str, float]:
        return self.client.getAllPrices()

    @overrides
    def getAccountBalance(self) -> Dict[str, float]:
        res = self.client.getAccountBalance()["balance"]
        res = {k:v[0] for k, v in res.items()}
        return res

    @overrides
    def swap(self, token1:str, token2:str, value1:float):
        assert False, "TODO"
