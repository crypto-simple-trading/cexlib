from overrides import overrides
from typing import Dict, List
from copy import deepcopy
from simple_exchange import SimpleExchange
from .exchange import Exchange

class FakeExchange(Exchange):
    def __init__(self, assets:List[str], prices:Dict[str, float], balance:Dict[str, float]):
        self.client = SimpleExchange(deepcopy(assets), deepcopy(prices), {"account":deepcopy(balance)})

    @overrides
    def getAllPrices(self) -> Dict[str, float]:
        newRes = {}
        res = self.client.getAllPrices()
        for k in res:
            for k2 in res[k]:
                newRes[f"{k}{k2}"] = res[k][k2]
        return newRes

    @overrides
    def getAccountBalance(self) -> Dict[str, float]:
        return self.client.getAccountBalance("account")

    @overrides
    def swap(self, token1:str, token2:str, value1:float):
        self.client.swap("account", token1, token2, value1)
